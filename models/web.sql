CREATE TABLE IF NOT EXISTS library (
  id INTEGER PRIMARY KEY,
  name TEXT COLLATE NOCASE,
  ext TEXT COLLATE NOCASE,
  file_id TEXT COLLATE NOCASE
);

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY,
  username TEXT,
  userid TEXT,
  email TEXT,
  regdate INTEGER,
  secret TEXT,
  confirmkey TEXT
);

INSERT OR IGNORE INTO users(id, username, userid, email, regdate, secret, confirmkey) VALUES (1,"admin", "dce5ab9f27f5f4607093afa93cc67713", "email@example.com", "1393273411887", "0c6fb3134776531844368e4d7853fdeebf2578b00de3f3ab86d072fe9dab3806", "723d76e155f9068d347f2f24963b37f7");
